/**
 * 适配器模式
 */
public class AdapterTest {

    public interface target{
        /* 源类Adaptee中有的方法 */
        public void sampleOperation1();
        /* 源类Adaptee中没有的方法 */
        public void sampleOperation2();
    }

    /* 测试接口的实现类 */
    public static class Target implements target{

        @Override
        public void sampleOperation1() {

        }

        @Override
        public void sampleOperation2() {

        }
    }

    /**
     * 源类Adaptee有方法sampleOperation1
     * 因此适配器类直接委派即可
     */
    public static interface Adaptee{
        public void sampleOperation1();
    }

    public static class Adapter{

        private Adaptee adaptee;

        public Adapter(Adaptee adaptee) {
            this.adaptee = adaptee;
        }

        public void sampleOperation1(){
            System.out.println("原有的sampleOperation1方法执行");
            this.adaptee.sampleOperation1();
        }

        public void sampleOperation2(){
            System.out.println("适配器类补充的sampleOperation2方法执行");
        }

    }

}
