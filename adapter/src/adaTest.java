public class adaTest {

    public static void main(String[] args) {

//        AdapterTest.Target target = new AdapterTest.Target();
//        target.sampleOperation1();
//        target.sampleOperation2();

        AdapterTest.Adaptee adaptee = new AdapterTest.Adaptee() {
            @Override
            public void sampleOperation1() {
                System.out.println("main方法实现adaptee接口，并重写sampleOperation1方法");
            }
        };
        AdapterTest.Adapter adapter = new AdapterTest.Adapter(adaptee);
        adapter.sampleOperation1();
        adapter.sampleOperation2();

    }

}
