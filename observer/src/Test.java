import java.util.Vector;

public class Test {

    public static void main(String[] args) {
        Vector students = new Vector();
        OBServer.Teacher teacher = new OBServer.Teacher();
        for (int i = 0; i < 10; i++) {
            OBServer.Student student = new OBServer.Student("zza" + i, teacher);
            students.add(student);
            teacher.attach(student);
        }

        System.out.println("Welcome to Andy.Chen Blog!" +"\n"
                +"Observer Patterns." +"\n"
                +"-------------------------------");

        teacher.setPhone("1234567");
        for (int i = 0; i < 3; i++) {
            ((OBServer.Student)students.get(i)).show();
        }

        teacher.setPhone("7654321");
        for (int i = 0; i < 3; i++) {
            ((OBServer.Student)students.get(i)).show();
        }

    }

}
