import java.util.Vector;

/**
 * 观察者模式
 * Subject类：它把所有对观察者对象的引用保存在一个聚集里，每个主题都可以有任何数量的观察着。抽象主题提供一个接口，可以增加和删除观察着对象。
 * Observer类：抽象观察者，为所有的具体观察者定义一个接口，在得到主题的通知时更新自己。
 * ConcreteSubject类：具体主题，将有关状态存入具体观察者对象；在具体主题的内部状态改变时，给所有登记过的观察者发出通知。
 * ConcreteObserver类：具体观察者，实现抽象观察者角色所要求的更新接口，以便使本身的状态与主题的状态相协调。
 */
public class OBServer {

    /**
     * Subject(目标，Subject)：
     * 目标知道它的观察者。可以有任意多个观察者观察同一个目标。
     * 提供注册和删除观察者对象的接口。
     */
    static interface Subject{
        public void attach(Observer mObserver);
        public void detach(Observer mObserver);
        public void notice();
    }

    /**
     * Observer(观察者，Observer)
     * 为那些在目标发生改变时需要获得通知的对象定义一个更新接口
     */
    static interface Observer{
        public void update();
    }

    /**
     * ConcreteSubject(具体目标，Teacher)
     * 将有关状态存入各ConcreteObserve对象。
     * 当他的状态发生改变时，向他的各个观察者发出通知。
     */
    public static class Teacher implements Subject{

        private String phone;
        private Vector students;

        public Teacher(){
            phone = "";
            students = new Vector();
        }

        @Override
        public void attach(Observer mObserver) {
            students.add(mObserver);
        }

        @Override
        public void detach(Observer mObserver) {
            students.remove(mObserver);
        }

        @Override
        public void notice() {
            for (int i = 0; i < students.size(); i++) {
                ((Observer)students.get(i)).update();
            }
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
            notice();
        }
    }

    /**
     * ConcreteObserver(具体观察者, Student)：
     * 维护一个指向ConcreteSubject对象的引用。
     * 存储有关状态，这些状态应与目标的状态保持一致。
     * 实现Observer的更新接口以使自身状态与目标的状态保持一致。
     */
    public static class Student implements Observer{

        private String name;
        private String phone;
        private Teacher mTeacher;

        public Student(String name,Teacher mTeacher){
            this.name = name;
            this.mTeacher = mTeacher;
        }

        public void show(){
            System.out.println("Name:"+name+ "\n" + "Teacher's phone" + phone);
        }

        @Override
        public void update() {
            phone = mTeacher.getPhone();
        }
    }

}
