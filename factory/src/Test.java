import java.util.Calendar;
import java.util.Locale;

public class Test {

    public static void main(String[] args) {
        //简单工厂创建两个对象，分别重写父类method()方法
        SimpleFactory.Sample sample1 = SimpleFactory.creator(1);
        sample1.method();
        SimpleFactory.Sample sample2 = SimpleFactory.creator(2);
        sample2.method();

        //还有一种目前比较流行的规范是把静态工厂方法命名为valueOf或者getInstance
        Integer integer = Integer.valueOf(100);
        Calendar instance = Calendar.getInstance(Locale.CHINA);
        System.out.println(integer+"-------------"+instance);

        /* ----------------------------------------------------------------------------------------- */

        //工厂方法实现
        //1.分别通过各自的工厂创建出工厂对象
        FactoryMethod.AoDiFactory aoDiFactory = new FactoryMethod.AoDiFactory();
        FactoryMethod.BmwFactory bmwFactory = new FactoryMethod.BmwFactory();
        //2.分别通过生成工厂对象的creatCat()方法生成各自的对象
        FactoryMethod.Car aoDi = aoDiFactory.creatCat();
        FactoryMethod.Car bmw = bmwFactory.creatCat();
        //3.分别调用对象中重写的run()方法
        aoDi.run();
        bmw.run();

        //Java中的具体实例
        //Collection
        //首先他是一个接口，它定义了一个方法，用于生产Iterator对象，即迭代器对象，
        // 那它的具体工厂是哪些呢，那实现了集合接口的有很多实现类，比如ArrayList。 ArrayList它去生产适合于ArrayList的迭代器对象。

        /* ----------------------------------------------------------------------------------------- */

        //抽象工厂方法的实现
        /* 创建总工厂对象实例，并通过它创建子工厂实例 */
        AbstractFactory.TotalFactory totalReallyFactory = new AbstractFactory.TotalReallyFactory();
        AbstractFactory.Car car = totalReallyFactory.createCar();
        car.run();
        AbstractFactory.Engine engine = totalReallyFactory.createEngine();
        engine.run();


    }

}
