public class FactoryMethod {

    /**
     * 创建工厂
     */
    public interface Car{
        public void run();
    }

    /**
     * 创建工厂方法调用接口（所有产品需要new出来必须继承他来实现方法）
     */
    public interface CarFactory{
        Car creatCat();
    }

    /* 创建方法调用的实例 */
    public static class AoDi implements Car{
        @Override
        public void run() {
            System.out.println("我是奥迪车");
        }
    }
    public static class Bmw implements Car{
        @Override
        public void run() {
            System.out.println("我是宝马车");
        }
    }

    /* 创建工厂方法调用接口的实例 */
    public static class AoDiFactory implements CarFactory{

        @Override
        public Car creatCat() {
            return new AoDi();
        }
    }
    public static class BmwFactory implements CarFactory{

        @Override
        public Car creatCat() {
            return new Bmw();
        }
    }

}
