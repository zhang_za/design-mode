public class AbstractFactory {

    /* 创建一个子工厂Car及其实现类CarA、CarB */
    public interface Car{
        public void run();
    }
    public static class CarA implements Car{

        @Override
        public void run() {
            System.out.println("宝马");
        }
    }
    public static class CarB implements Car{

        @Override
        public void run() {
            System.out.println("奥迪");
        }
    }

    /* 创建第二个子工厂Engine及其实现类EngineA、EngineB */
    public interface Engine{
        public void run();
    }
    public static class EngineA implements Engine{

        @Override
        public void run() {
            System.out.println("转的快");
        }
    }
    public static class EngineB implements Engine{

        @Override
        public void run() {
            System.out.println("转的慢");
        }
    }

    /* 创建一个总工厂TotalFactory及其实现类TotalReallyFactory，并由总工厂决定调用哪个子工厂实例 */
    public interface TotalFactory{
        //创建汽车
        Car createCar();
        //创建发动机
        Engine createEngine();
    }
    public static class TotalReallyFactory implements TotalFactory{
        @Override
        public Car createCar() {
            return new CarA();
        }
        @Override
        public Engine createEngine() {
            return new EngineB();
        }
    }

}
