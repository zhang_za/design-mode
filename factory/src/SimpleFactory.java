/**
 * 简单工厂模式
 * 目的：定义一个用于创建对象的接口
 * 组成：工厂类角色：这是本模式的核心，含有一定的商业逻辑和判断逻辑。在java中它往往由一个具体类实现。
 *      抽象产品角色：它一般是具体产品继承的父类或者实现的接口。在java中由接口或者抽象类来实现。
 *      具体产品角色：工厂类所创建的对象就是此角色的实例。在java中由一个具体类实现。
 */
public class SimpleFactory {

    public static Sample creator(int which) {

        if (which == 1)
            return new SampleA();
        else
            return new SampleB();

    }

    /**
     * 使用内部类来模拟需要实现的类
     */
    public static class Sample{
        protected void method(){
            System.out.println("工厂创建对象成功。。。");
        }
    }

    public static class SampleA extends Sample{
        protected void method(){
            System.out.println("SampleA对象创建成功");
        }
    }

    public static class SampleB extends Sample{
        protected void method(){
            System.out.println("SampleB对象创建成功");
        }
    }

}
