/**
 * 装饰模式
 * Component是定义一个对象接口，可以给这些对象动态地添加职责。
 * ConcreteComponent是定义了一个具体的对象，也可以给这个对象添加一些职责。
 *
 * Decorator是装饰抽象类，继承了Component,从外类来扩展Component类的功能，但对于Component来说，是无需知道Decorator存在的。
 * ConcreteDecorator就是具体的装饰对象，起到给Component添加职责的功能。
 */
public class DecoratorTest {

    public interface Component{
        void show();
    }

    public static class Person implements Component{

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Person(String name) {
            this.name = name;
        }

        @Override
        public void show() {
            System.out.println("装扮的" + name);
        }
    }

    /**
     * 创建装饰类 DecoratorTest 实现 Component 接口
     */
    public static class Decorator implements Component{

        private Component component;

        public void decoratorObj(Component component){
            this.component = component;
        }

        @Override
        public void show() {
            if (this.component != null){
                this.component.show();
            }
        }
    }

    /* 分别创建具体的修饰类，继承Decorator */
    public static class Show1 extends Decorator{

        @Override
        public void show() {
            System.out.println("行为1");
            super.show();
        }
    }
    public static class Show2 extends Decorator{

        @Override
        public void show() {
            System.out.println("行为2");
            super.show();
        }
    }
    public static class Show3 extends Decorator{

        @Override
        public void show() {
            System.out.println("行为3");
            super.show();
        }
    }

}
