public class DecMainTest {

    public static void main(String[] args) {

        DecoratorTest.Person person = new DecoratorTest.Person("小明");

        DecoratorTest.Show1 show1 = new DecoratorTest.Show1();
        DecoratorTest.Show2 show2 = new DecoratorTest.Show2();
        DecoratorTest.Show3 show3 = new DecoratorTest.Show3();

        show1.decoratorObj(person);
        show2.decoratorObj(show1);
        show3.decoratorObj(show2);

//        person.show();
        show3.show();

    }

}
