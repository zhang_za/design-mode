import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CGLIBProxy {

    /* 创建被代理的接口及其实现类 */
    public interface UserDao {
        public void save();
    }

    public static class UserDaoImpl implements UserDao {
        @Override
        public void save() {
            System.out.println("数据保存成功");
        }
    }

    /* 代理主要类 */
    public static class CglibProxy implements MethodInterceptor{
        private Object object;
        // 这里的目标类型为Object，则可以接受任意一种参数作为被代理类，实现了动态代理
        public Object getInstance(Object object){
            this.object = object;
            Enhancer enhancer = new Enhancer();
            enhancer.setSuperclass(object.getClass());
            enhancer.setCallback(this);
            return enhancer.create();
        }

        //代理的实际方法
        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            System.out.println("开启事务");
            Object invoke = methodProxy.invoke(object, objects);
            System.out.println("关闭事务");
            return invoke;
        }
    }



}
