import java.lang.reflect.Proxy;

public class Test {

    public static void main(String[] args) {
        System.out.println("静态代理开始...");
        //静态代理
        StaticProxy.User user = new StaticProxy.User();
        user.save();
        //在不改变user类的情况下开启事务和关闭事务
        //需要用到代理(静态代理)
        StaticProxy.UserProxy userProxy = new StaticProxy.UserProxy(user);
        userProxy.save();

        /* ----------------------------------------------------------------------------------------- */

        System.out.println("JDK动态代理开始。。。");
        //JDK动态代理
        JDKProxy.User user1 = new JDKProxy.User();
        user1.save();
        JDKProxy.InvocationHandlerImpl invocationHandler = new JDKProxy.InvocationHandlerImpl(user1);
        //类加载器
        ClassLoader classLoader = user1.getClass().getClassLoader();
        Class<?>[] interfaces = user1.getClass().getInterfaces();
        //主要装载器、一组接口及调用处理动态代理实例
        JDKProxy.UserDao user2 = (JDKProxy.UserDao) Proxy.newProxyInstance(classLoader, interfaces, invocationHandler);
        user2.save();

        /* ----------------------------------------------------------------------------------------- */

        System.out.println("Cglib动态代理开始。。。");
        //Cglib动态代理
        CGLIBProxy.CglibProxy cglibProxy = new CGLIBProxy.CglibProxy();
        CGLIBProxy.UserDao userDao = (CGLIBProxy.UserDao) cglibProxy.getInstance(new CGLIBProxy.UserDaoImpl());
        userDao.save();

    }

}
