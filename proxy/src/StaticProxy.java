/**
 * 静态代理
 * 缺点：每个需要代理的对象都需要自己重复编写代理，很不舒服
 * 优点：但是可以面相实际对象或者是接口的方式实现代理
 */
public class StaticProxy {

    public static class User{
        public void save(){
            System.out.println("保存数据成功");
        }
    }

    public static class UserProxy extends User{
        private User user;
        public UserProxy(User user){
            this.user = user;
        }
        public void save(){
            System.out.println("开启事务。。。");
            user.save();
            System.out.println("关闭事务。。。");
        }
    }


}
