import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * JDK动态代理
 * 动态代理的对象，是利用JDK的API，动态的在内存中构建代理对象（是根据被代理的接口来动态生成代理类的class文件，并加载运行的过程），这就叫动态代理JDK
 * 代理 : 基于接口的动态代理技术：利用拦截器（必须实现invocationHandler）加上反射机制生成一个代理接口的匿名类，在调用具体方法前调用InvokeHandler来处理，从而实现方法增强
 * 缺点：必须是面向接口，目标业务类必须实现接口
 * 优点：不用关心代理类，只需要在运行阶段才指定代理哪一个对象
 */
public class JDKProxy {

    /* 创建被代理的接口及其实现类 */
    public interface UserDao {
        public void save();
    }

    public static class User implements UserDao {
        @Override
        public void save() {
            System.out.println("数据保存成功");
        }
    }

    /* 动态代理类，可以重复使用，不像静态代理那样要自己重复编写代码 */
    //每次生成动态代理类对象时,实现了InvocationHandler接口的调用处理器对象
    public static class InvocationHandlerImpl implements InvocationHandler {

        //这其实业务实现类对象，用来调用具体的业务方法
        private Object object;

        //通过构造函数传入目标对象
        public InvocationHandlerImpl(Object object) {
            this.object = object;
        }
        //动态代理实际运行的代理方法
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("调用开始处理");
            //下面invoke()方法是以反射的方式来创建对象，第一个参数是要创建的对象，第二个是构成方法的参数，由第二个参数来决定创建对象使用哪个构造方法
            Object result = method.invoke(object, args);
            System.out.println("调用结束处理");
            return result;
        }
    }


}
