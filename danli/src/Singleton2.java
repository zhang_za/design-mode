/**
 * 懒汉式
 * 优点：延迟加载（需要的时候才去加载），适合单线程操作
 * 缺点：线程不安全，在多线程中很容易出现不同步的情况，
 */
public class Singleton2 {

    /* 持有私有静态实例，防止被引用，此处赋值为null，目的是实现延迟加载 */
    private static Singleton2 instance = null;

    /* 私有构造方法，防止被实例化 */
    private Singleton2(){

    }

    public static Singleton2 getInstance(){
        if (instance == null){
            instance = new Singleton2();
        }
        return instance;
    }

    protected void method(){
        System.out.println("懒汉加载：SingletonInner。。。。");
    }

}
