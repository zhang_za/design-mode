public class Test {

    public static void main(String[] args) {
        //饿汉式
        Singleton instance1 = Singleton.getInstance();
        instance1.method();
        //懒汉式
        Singleton2 instance2 = Singleton2.getInstance();
        instance2.method();
        //双重线程检查模式
        Singleton3 instance3 = Singleton3.getInstance();
        instance3.method();
        //内部类模式
        Singleton4 instance4 = Singleton4.getInstance();
        instance4.method();
    }

}
