/**
 * 饿汉式
 */
public class Singleton {

    private static Singleton singleton = new Singleton();

    private Singleton(){

    }

    public static Singleton getInstance(){
        return singleton;
    }

    protected void method(){
        System.out.println("饿汉加载：SingletonInner。。。。");
    }

}
