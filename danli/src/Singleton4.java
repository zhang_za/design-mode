/**
 * 内部类实现
 */
public class Singleton4 {

    /**
     * 内部类实现单例模式
     * 延迟加载  减少内存开销
     */
    private static class Singleton {
        private static Singleton4 instance = new Singleton4();
    }

    /**
     * 私有的构造方法
     */
    private Singleton4(){

    }

    public static Singleton4 getInstance(){
        return Singleton.instance;
    }

    protected void method(){
        System.out.println("内部类实现加载：SingletonInner。。。。");
    }

}
