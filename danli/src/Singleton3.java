/**
 * 双重线程检查模式
 * 优点：线程安全，支持延时加载，调用效率高
 * 缺点：写法复杂，不简洁
 */
public class Singleton3 {

    /**
     * volatile的作用
     * 1、保证可见性；
     * 2、防止指令重排；
     * 3、但是不保证原子性；（原子性靠synchronized保证）
     */
    private static volatile Singleton3 instance = null;

    /* 私有的构造函数 */
    private Singleton3(){

    }

    public static Singleton3 getInstance(){
        if (instance == null){
            synchronized (Singleton3.class){
                if (instance == null){
                    instance = new Singleton3();
                }
            }
        }
        return instance;
    }

    protected void method(){
        System.out.println("双重线程检查模式实现加载：SingletonInner。。。。");
    }

}
