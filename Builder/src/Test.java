public class Test {

    public static void main(String[] args) {

        Builder.ConcreteBuilder concreteBuilder = new Builder.ConcreteBuilder();
        Builder.Director director = new Builder.Director(concreteBuilder);
        director.construct();
        concreteBuilder.getResult();

    }

}
