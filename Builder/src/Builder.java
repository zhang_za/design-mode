/**
 * 建造者模式
 * Builder：为创建一个Product对象的各个部件指定抽象接口。
 * ConcreteBuilder：实现Builder的接口以构造和装配该产品的各个部件，定义并明确它所创建的表示，提供一个检索产品的接口
 * Director：构造一个使用Builder接口的对象。
 * Product：表示被构造的复杂对象。ConcreateBuilder创建该产品的内部表示并定义它的装配过程。
 */
public class Builder {

    /* 复杂的商品类 */
    public static class Product{
        private String a;
        private String b;
        private String c;

        public String getA() {
            return a;
        }

        public void setA(String a) {
            this.a = a;
        }

        public String getB() {
            return b;
        }

        public void setB(String b) {
            this.b = b;
        }

        public String getC() {
            return c;
        }

        public void setC(String c) {
            this.c = c;
        }
    }


    /* 创建一个接口，定义如何创建复杂对象的各个组件 */
    public interface Builders{
        //创建部件A
        void buildPartA();
        //创建部件B
        void buildPartB();
        //创建部件C
        void buildPartC();

        //返回最后组装成品结果 (返回最后装配好的汽车)
        //成品的组装过程不在这里进行,而是转移到下面的Director类中进行.

        //从而实现了解耦过程和部件
        Product getResult();

    }

    /* 用Director构建最后的复杂对象，而在上面Builder接口中封装的是如何创建一个个部件(复杂对象是由这些部件组成的)，也就是说Director的内容是如何将部件最后组装成成品 */
    public static class Director{
        private Builders builders;

        public Director(Builders builders){
            this.builders = builders;
        }

        // 将部件partA partB partC最后组成复杂对象
        public void construct(){
            builders.buildPartA();
            builders.buildPartB();
            builders.buildPartC();
        }

    }

    /* Builder的具体实现ConcreteBuilder */
    /**
     * 通过具体完成接口Builder来构建或装配产品的部件；
     * 定义并明确它所要创建的是什么具体东西；
     * 提供一个可以重新获取产品的接口。
     */
    public static class ConcreteBuilder implements Builders{

        Product product = new Product();

        @Override
        public void buildPartA() {
            product.setA("a");
            System.out.println("product创建A");
        }

        @Override
        public void buildPartB() {
            product.setB("b");
            System.out.println("product创建A");
        }

        @Override
        public void buildPartC() {
            product.setC("c");
            System.out.println("product创建A");
        }

        @Override
        public Product getResult() {
            System.out.println("product,a:" + this.product.getA()+",b:"+this.product.getB()+",c:"+this.product.getC());
            return this.product;

        }
    }

}
